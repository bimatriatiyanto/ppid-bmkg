@extends('frontend/master')
@section('content')
<section class="wrapper bg-light">
    <div class="container pt-8 pt-md-14">
        <div class="row">
            <div class="col-lg-12">
                <div class="swiper-container blog   grid-view mb-15 " data-margin="30" data-nav="true"
                    data-dots="true" data-items-xxl="4" data-items-md="3" data-items-xs="2" data-effect="slide"
                    data-autoplay="true">
                    <div class="swiper">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <figure class="rounded bg-pale-aqua p-1">

                                    <a href="https://www.wmo.int/" target="_blank">
                                        <img src="https://bmkg.go.id/asset/img/banner/banner-wmo.jpg" alt="WMO">
                                    </a>



                                </figure>
                            </div>
                            <!--/.swiper-slide -->
                            <div class="swiper-slide">
                                <figure class="rounded bg-pale-aqua p-1">
                                    <a href="http://epengawasan.bmkg.go.id/wbs/" target="_blank">
                                        <img src="https://bmkg.go.id/asset/img/banner/banner-epengawasan.jpg"
                                            alt="E-Pengawasan">
                                    </a>
                                </figure>
                            </div>
                            <!--/.swiper-slide -->
                            <div class="swiper-slide">
                                <figure class="rounded bg-pale-aqua p-1">
                                    <a href="https://www.lapor.go.id/" target="_blank">
                                        <img src="https://bmkg.go.id/asset/img/banner/Lapor-UKP4.jpg"
                                            alt="LAPOR!">
                                    </a>
                                </figure>
                            </div>
                            <!--/.swiper-slide -->
                            <div class="swiper-slide">
                                <figure class="rounded bg-pale-aqua p-1">
                                    <a href="http://bit.ly/PersepsiWebsiteBMKG">
                                        <img src="https://bmkg.go.id/asset/img/banner/Banner-Survey-BMKG.jpg"
                                            alt="Survey Layanan Website BMKG">
                                    </a>
                                </figure>
                            </div>
                            <!--/.swiper-slide -->
                            <div class="swiper-slide">
                                <figure class="rounded bg-pale-aqua p-1">
                                    <a href="https://www.bmkg.go.id/rb" target="_blank">
                                        <img src="https://bmkg.go.id/asset/img/banner/banner-reformasi-birokrasi.jpg"
                                            alt="Reformasi Birokrasi BMKG">
                                    </a>
                                </figure>
                            </div>
                            <!--/.swiper-slide -->
                            <div class="swiper-slide">
                                <figure class="rounded bg-pale-aqua p-1">
                                    <a href="https://www.bmkg.go.id/profil/?p=stop-pungli-bmkg" target="_blank">
                                        <img src="https://bmkg.go.id/asset/img/banner/saber-pungli.jpg"
                                            alt="Saber Pungli">
                                    </a>
                                </figure>
                            </div>
                            <!--/.swiper-slide -->
                            <div class="swiper-slide">
                                <figure class="rounded bg-pale-aqua p-1">
                                    <a href="http://dataonline.bmkg.go.id/home" target="_blank">
                                        <img src="https://bmkg.go.id/asset/img/banner/banner_data_online.jpg"
                                            alt="Data Online">
                                    </a>
                                </figure>
                            </div>
                            <div class="swiper-slide">
                                <figure class="rounded bg-pale-aqua p-1">

                                    <a href="http://puslitbang.bmkg.go.id/jmg" target="_blank">
                                        <img src="https://bmkg.go.id/asset/img/banner/banner-journalMG.jpg"
                                            alt="Jurnal Meteorologi dan Geofisika">
                                    </a>
                                </figure>
                            </div>
                            <div class="swiper-slide">
                                <figure class="rounded bg-pale-aqua p-1">
                                    <a href="http://inatews.bmkg.go.id/new/query_gmpqc.php" target="_blank">
                                        <img src="https://bmkg.go.id/asset/img/banner/dataGempa.jpg"
                                            alt="Data Gempabumi">
                                    </a>
                                </figure>
                            </div>
                            <div class="swiper-slide">
                                <figure class="rounded bg-pale-aqua p-1">
                                    <a href="https://www.bmkg.go.id/geofisika-potensial/kalkulator-magnet-bumi.bmkg"
                                        target="_blank">
                                        <img src="https://bmkg.go.id/asset/img/banner/kalkulator-magnet.jpg"
                                            alt="Kalkulator Magnet Bumi">
                                    </a>
                                    </figu>
                            </div>
                            <div class="swiper-slide">
                                <figure class="rounded bg-pale-aqua p-1">
                                    <a href="#">
                                        <img src="https://bmkg.go.id/asset/img/banner/mottobmkg.jpg"
                                            alt="Motto BMKG">
                                    </a>
                                </figure>
                            </div>
                            <div class="swiper-slide">
                                <figure class="rounded bg-pale-aqua p-1">
                                    <a href="https://www.bmkg.go.id/profil/?p=maklumat-pelayanan">
                                        <img src="https://bmkg.go.id/asset/img/banner/maklumatpelayanan.jpg"
                                            alt="Maklumat Pelayanan">
                                    </a>
                                </figure>
                            </div>
                            <div class="swiper-slide">
                                <figure class="rounded bg-pale-aqua p-1">
                                    <a href="http://lpse.bmkg.go.id" target="_blank">
                                        <img src="https://bmkg.go.id/asset/img/banner/lpse.jpg" alt="LPSE">
                                    </a>
                                </figure>
                            </div>
                            <!--/.swiper-slide -->
                        </div>
                        <!--/.swiper-wrapper -->
                    </div>
                    <!-- /.swiper -->
                </div>
                <!-- /.swiper-container -->
            </div>

        </div>
        <div
            class="row gx-lg-0 gx-xl-8 mb-5 mb-md-7 mb-lg-14 justify-content-center align-items-center">
            <div class="col-md-6 col-xl-4">
                <div class="card shadow-lg card-border-bottom border-soft-primary">
                    <div class="card-body p-4">
                        <img src="./assets/img/icons/lineal/browser.svg"
                            class="svg-inject icon-svg icon-svg-md text-yellow mb-3" alt="" />
                        <h4>Informasi Publik Setiap Saat</h4>
                        {{-- <p class="mb-2">Nulla vitae elit libero, a pharetra augue. Donec id elit non
                            mi porta gravida at eget metus cras justo.</p> --}}
                        <a href="#" class="more hover link-primary">Selengkapnya</a>
                    </div>
                    <!--/.card-body -->
                </div>
                <!--/.card -->
            </div>
            <!--/column -->
            <div class="col-md-6 col-xl-4">
                <div class="card shadow-lg card-border-bottom border-soft-primary">
                    <div class="card-body p-4">
                        <img src="./assets/img/icons/lineal/check-list.svg"
                            class="svg-inject icon-svg icon-svg-md text-yellow mb-3" alt="" />
                        <h4>Informasi Publik Secara Berkala</h4>
                        {{-- <p class="mb-2">Nulla vitae elit libero, a pharetra augue. Donec id elit non
                            mi porta gravida at eget metus cras justo.</p> --}}
                        <a href="#" class="more hover link-primary">Selengkapnya</a>
                    </div>
                    <!--/.card-body -->
                </div>
                <!--/.card -->
            </div>
            <!--/column -->
            <div class="col-md-6 col-xl-4">
                <div class="card shadow-lg card-border-bottom border-soft-primary">
                    <div class="card-body p-4">
                        <img src="./assets/img/icons/lineal/analytics.svg"
                            class="svg-inject icon-svg icon-svg-md text-yellow mb-3" alt="" />
                        <h4>Informasi Publik Serta Merta</h4>
                        {{-- <p class="mb-2">Nulla vitae elit libero, a pharetra augue. Donec id elit non
                            mi porta gravida at eget metus cras justo.</p> --}}
                        <a href="#" class="more hover link-primary">Selengkapnya</a>
                    </div>
                    <!--/.card-body -->
                </div>
                <!--/.card -->
            </div>
            <!--/column -->

        </div>
        <!-- /.row -->
        <!--/.row -->
        {{-- <div class="row">
            <div class="col-lg-9 col-xl-8 col-xxl-7 mx-auto text-center">
                <h2 class="display-6 text-uppercase  mb-3">Layanan BMKG</h2>

            </div>
            <!-- /column -->
        </div> --}}
        <!-- /.row -->
        <!-- /.container -->


        <div class="container-xxl bg-green text-white rounded">
            <div
                class="row gx-lg-8  gx-xl-12 py-8   justify-content-center align-items-center  ">
                <div class="col-lg-3 position-relative">
                    <div class="shape bg-dot primary rellax w-17 h-18" data-rellax-speed="1"
                        style="bottom: -2rem; left: -0.7rem;"></div>
                    <figure class="d-none d-lg-block rounded mb-0 "><img src="{{ url('/images/ppid-banner.png') }}" alt="">
                    </figure>
                </div>
                <!--/column -->
                <div class="col-lg-6 mt-12 px-md-8">
                    <h3 class="display-6 mb-4 text-white">Tentang PPID BMKG</h3>
                    <p class="mb-4">Cum sociis natoque penatibus et magnis dis parturient montes,
                        nascetur ridiculus mus. Cras justo odio, dapibus ac facilisis in, egestas eget quam.
                        Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Maecenas faucibus
                        mollis interdum. Maecenas sed diam eget risus varius.</p>

                    <a href="#" class="btn btn-primary rounded-pill  mb-0">Lihat Profil</a>
                </div>
                <!--/column -->
            </div>
            <div class="row justify-content-center align-items-center py-8 px-4">
                <div class="col-lg-6 bg-soft-green text-dark text-center rounded p-4 ">
                    <div class="display-6 mb-3">Ajukan Permohonan Informasi</div>
                    <div class="my-2">Lorem ipsum dolor sit amet consectetur adipisicing elit. Ab, eius numquam ipsam officiis molestiae saepe.</div>
                    <div>
                        <a href="#" class="btn btn-primary rounded-pill">Ajukan Permohonan</a>
                    </div>

                </div>
            </div>

        </div>

        <div class="container pt-12 pt-md-10 pb-8 pb-md-6">

            <div class="swiper-container grid-view mb-6" data-margin="30" data-dots="true" data-items-xl="5" data-items-md="3" data-items-xs="2">
              <div class="swiper">
                <div class="swiper-wrapper">
                  <div class="swiper-slide">
                    <figure class="rounded mb-6"><img src="../../assets/img/photos/pd7.jpg" srcset="../../assets/img/photos/pd7@2x.jpg 2x" alt="" /><a class="item-link swiper-no-swiping" href="../../assets/img/photos/pd7-full.jpg" data-glightbox data-gallery="projects-group"><i class="uil uil-focus-add"></i></a></figure>
                    <div class="project-details d-flex justify-content-center flex-column">
                      <div class="post-header">
                        <h2 class="post-title h3"><a href="../../single-project.html" class="link-dark">Cras Fermentum Sem</a></h2>
                        <div class="post-category text-ash">Stationary</div>
                      </div>
                      <!-- /.post-header -->
                    </div>
                    <!-- /.project-details -->
                  </div>
                  <!--/.swiper-slide -->
                  <div class="swiper-slide">
                    <figure class="rounded mb-6"><img src="../../assets/img/photos/pd8.jpg" srcset="../../assets/img/photos/pd8@2x.jpg 2x" alt="" /><a class="item-link swiper-no-swiping" href="../../assets/img/photos/pd8-full.jpg" data-glightbox data-gallery="projects-group"><i class="uil uil-focus-add"></i></a></figure>
                    <div class="project-details d-flex justify-content-center flex-column">
                      <div class="post-header">
                        <h2 class="post-title h3"><a href="../../single-project2.html" class="link-dark">Mollis Ipsum Mattis</a></h2>
                        <div class="post-category text-ash">Magazine, Book</div>
                      </div>
                      <!-- /.post-header -->
                    </div>
                    <!-- /.project-details -->
                  </div>
                  <!--/.swiper-slide -->
                  <div class="swiper-slide">
                    <figure class="rounded mb-6"><img src="../../assets/img/photos/pd9.jpg" srcset="../../assets/img/photos/pd9@2x.jpg 2x" alt="" /><a class="item-link swiper-no-swiping" href="../../assets/img/photos/pd9-full.jpg" data-glightbox data-gallery="projects-group"><i class="uil uil-focus-add"></i></a></figure>
                    <div class="project-details d-flex justify-content-center flex-column">
                      <div class="post-header">
                        <h2 class="post-title h3"><a href="../../single-project3.html" class="link-dark">Ipsum Ultricies Cursus</a></h2>
                        <div class="post-category text-ash">Packaging</div>
                      </div>
                      <!-- /.post-header -->
                    </div>
                    <!-- /.project-details -->
                  </div>
                  <!--/.swiper-slide -->
                  <div class="swiper-slide">
                    <figure class="rounded mb-6"><img src="../../assets/img/photos/pd10.jpg" srcset="../../assets/img/photos/pd10@2x.jpg 2x" alt="" /><a class="item-link swiper-no-swiping" href="../../assets/img/photos/pd10-full.jpg" data-glightbox data-gallery="projects-group"><i class="uil uil-focus-add"></i></a></figure>
                    <div class="project-details d-flex justify-content-center flex-column">
                      <div class="post-header">
                        <h2 class="post-title h3"><a href="../../single-project.html" class="link-dark">Inceptos Euismod Egestas</a></h2>
                        <div class="post-category text-ash">Stationary, Branding</div>
                      </div>
                      <!-- /.post-header -->
                    </div>
                    <!-- /.project-details -->
                  </div>
                  <!--/.swiper-slide -->
                  <div class="swiper-slide">
                    <figure class="rounded mb-6"><img src="../../assets/img/photos/pd11.jpg" srcset="../../assets/img/photos/pd11@2x.jpg 2x" alt="" /><a class="item-link swiper-no-swiping" href="../../assets/img/photos/pd11-full.jpg" data-glightbox data-gallery="projects-group"><i class="uil uil-focus-add"></i></a></figure>
                    <div class="project-details d-flex justify-content-center flex-column">
                      <div class="post-header">
                        <h2 class="post-title h3"><a href="../../single-project2.html" class="link-dark">Ipsum Mollis Vulputate</a></h2>
                        <div class="post-category text-ash">Packaging</div>
                      </div>
                      <!-- /.post-header -->
                    </div>
                    <!-- /.project-details -->
                  </div>
                  <!--/.swiper-slide -->
                  <div class="swiper-slide">
                    <figure class="rounded mb-6"><img src="../../assets/img/photos/pd12.jpg" srcset="../../assets/img/photos/pd12@2x.jpg 2x" alt="" /><a class="item-link swiper-no-swiping" href="../../assets/img/photos/pd12-full.jpg" data-glightbox data-gallery="projects-group"><i class="uil uil-focus-add"></i></a></figure>
                    <div class="project-details d-flex justify-content-center flex-column">
                      <div class="post-header">
                        <h2 class="post-title h3"><a href="../../single-project3.html" class="link-dark">Porta Ornare Cras</a></h2>
                        <div class="post-category text-ash">Branding</div>
                      </div>
                      <!-- /.post-header -->
                    </div>
                    <!-- /.project-details -->
                  </div>
                  <!--/.swiper-slide -->
                </div>
                <!--/.swiper-wrapper -->
              </div>
              <!-- /.swiper -->
            </div>
            <!-- /.swiper-container -->
          </div>




    </div>


</section>
@endsection
