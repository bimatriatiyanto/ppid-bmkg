@if ($mode == 'create')
    @include('brackets/admin-ui::admin.includes.media-uploader', [
        'mediaCollection' => app(App\Models\Document::class)->getMediaCollection('berkas'),
        'label' => 'Document PPID',
    ])
@else
    @include('brackets/admin-ui::admin.includes.media-uploader', [
        'mediaCollection' => app(App\Models\Document::class)->getMediaCollection('berkas'),
        'media' => $document->getThumbs200ForCollection('berkas'),
        'label' => 'Document PPID',
    ])
@endif
