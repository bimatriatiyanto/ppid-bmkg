@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.document.actions.create'))

@section('body')

    <div class="container-xl">

        <document-form :action="'{{ url('admin/documents') }}'" :categories="{{ $categories->toJson() }}" v-cloak
            inline-template>

            <form class="form-horizontal form-create" method="post" @submit.prevent="onSubmit" :action="action" novalidate>

                <div class="row">
                    <div class="col">
                        @include('admin.document.components.form-elements')
                    </div>

                    <div class="col-md-12 col-lg-12 col-xl-5 col-xxl-4">
                        @include(
                            'admin.document.components.form-elements-right'
                        )
                    </div>
                </div>
                @include('admin.document.components.form-upload')

                <button type="submit" class="btn btn-primary fixed-cta-button" :disabled="submiting">
                    <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-save'"></i>
                    {{ trans('brackets/admin-ui::admin.btn.save') }}
                </button>

            </form>

        </document-form>

    </div>


@endsection
