<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description"
        content="An impressive and flawless site template that includes various UI elements and countless features, attractive ready-made blocks and rich pages, basically everything you need to create a unique and professional website.">
    <meta name="keywords"
        content="bootstrap 5, business, corporate, creative, gulp, marketing, minimal, modern, multipurpose, one page, responsive, saas, sass, seo, startup, html5 template, site template">
    <meta name="author" content="elemis">
    <title>E-PPID BMKG</title>
    <link rel="shortcut icon" href="{{ url('/images/logo-bmkg.png') }}">
    <link rel="stylesheet" href="./assets/css/plugins.css">
    <link rel="stylesheet" href="./assets/css/style.css">
    {{-- <link rel="preload" href="./assets/css/fonts/dm.css" as="style" onload="this.rel='stylesheet'"> --}}
    <link rel="preload" href="./assets/css/fonts/thicccboi.css" as="style" onload="this.rel='stylesheet'">
    <link rel="stylesheet" href="{{ url('css/custom.css') }}">
</head>

<body>
    <div class="content-wrapper">
        <header class="wrapper bg-light">
            <div class="d-none d-lg-block bg-navy text-white fw-bold fs-15 mb-2 ">
                <div class="container py-2 d-md-flex flex-md-row">
                    <div class="d-flex flex-row align-items-center">
                        <div class="icon text-white fs-22 mt-1 me-2"> <i class="uil uil-location-pin-alt"></i></div>
                        <address class="mb-0">Jl. Angkasa I No.2 Kemayoran
                            Jakarta Pusat, DKI Jakarta</address>
                    </div>
                    <div class="d-flex flex-row align-items-center me-6 ms-auto">
                        <div class="icon text-white fs-22 mt-1 me-2"> <i class="uil uil-phone-volume"></i></div>
                        <p class="mb-0">Call Center (196)</p>
                    </div>
                    <div class="d-flex flex-row align-items-center">
                        <div class="icon text-white fs-22 mt-1 me-2"> <i class="uil uil-message"></i></div>
                        <p class="mb-0"><a href="mailto:info@bmkg.go.id"
                                class="link-white hover">info@bmkg.go.id</a></p>
                    </div>
                </div>
                <!-- /.container -->
            </div>
            <nav class="navbar navbar-expand-lg center-nav transparent navbar-light">
                <div class="container flex-lg-row flex-nowrap align-items-center">
                    <div class="navbar-brand w-100 ">
                        <a class="d-flex flex-row align-items-center" href="./index.html">
                            <img class="img-brand" src="{{ url('images/logo-bmkg.png') }}" alt="" />
                            <h3 class="ms-4">E-PPID BMKG</h3>
                        </a>
                    </div>
                    <div class="navbar-collapse offcanvas offcanvas-nav offcanvas-start">
                        <div class="offcanvas-header d-lg-none">
                            <h3 class="text-white fs-30 mb-0">E-PPID BMKG</h3>
                            <button type="button" class="btn-close btn-close-white" data-bs-dismiss="offcanvas"
                                aria-label="Close"></button>
                        </div>
                        <div class="offcanvas-body ms-lg-auto d-flex flex-column h-100">
                            <ul class="navbar-nav">
                                <li class="nav-item dropdown dropdown-mega">
                                    <a class="nav-link " href="#">Beranda</a>

                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" data-bs-toggle="dropdown">Profil</a>
                                    <ul class="dropdown-menu">

                                        <li class="nav-item"><a class="dropdown-item"
                                                href="./pricing.html">Sejarah</a></li>
                                        <li class="nav-item"><a class="dropdown-item" href="./onepage.html">Visi
                                                dan Misi</a></li>
                                        <li class="nav-item"><a class="dropdown-item" href="./onepage.html">Tugas
                                                & Fungsi</a></li>
                                        <li class="nav-item"><a class="dropdown-item"
                                                href="./onepage.html">Struktur Organisasi</a></li>
                                    </ul>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" data-bs-toggle="dropdown">Informasi
                                        Publik</a>
                                    <ul class="dropdown-menu">
                                        <li class="nav-item"><a class="dropdown-item"
                                                href="./onepage.html">Informasi Publik Secara Berkala</a></li>
                                        <li class="nav-item"><a class="dropdown-item"
                                                href="./onepage.html">Informasi Publik Setiap Saat</a></li>
                                        <li class="nav-item"><a class="dropdown-item"
                                                href="./onepage.html">Informasi Publik Serta Merta</a></li>
                                    </ul>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link" href="#">Laporan</a>

                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link" href="#">PTSP</a>

                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link" href="#">Penghargaan</a>

                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link" href="#">FAQ</a>

                                </li>

                            </ul>
                            <!-- /.navbar-nav -->
                            <div class="offcanvas-footer d-lg-none">
                                <div>
                                    <a href="mailto:first.last@email.com" class="link-inverse">info@email.com</a>
                                    <br /> 00 (123) 456 78 90 <br />
                                    <nav class="nav social social-white mt-4">
                                        <a href="#"><i class="uil uil-twitter"></i></a>
                                        <a href="#"><i class="uil uil-facebook-f"></i></a>
                                        <a href="#"><i class="uil uil-dribbble"></i></a>
                                        <a href="#"><i class="uil uil-instagram"></i></a>
                                        <a href="#"><i class="uil uil-youtube"></i></a>
                                    </nav>
                                    <!-- /.social -->
                                </div>
                            </div>
                            <!-- /.offcanvas-footer -->
                        </div>
                        <!-- /.offcanvas-body -->
                    </div>
                    <!-- /.navbar-collapse -->
                    <div class="navbar-other w-100 d-flex ms-auto d-lg-none">
                        <ul class="navbar-nav flex-row align-items-center ms-auto">

                            {{-- <li class="nav-item d-none d-md-block">
                                <a href="./contact.html" class="btn btn-sm btn-primary rounded-pill">Contact</a>
                            </li> --}}
                            <li class="nav-item d-lg-none">
                                <button class="hamburger offcanvas-nav-btn"><span></span></button>
                            </li>
                        </ul>
                        <!-- /.navbar-nav -->
                    </div>
                    <!-- /.navbar-other -->
                </div>
                <!-- /.container -->
            </nav>
            <!-- /.navbar -->
        </header>
        <!-- /header -->
        <section class="wrapper bg-light">
            <div class="container pt-8 pt-md-14">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="swiper-container blog   grid-view mb-15 " data-margin="30" data-nav="true"
                            data-dots="true" data-items-xxl="4" data-items-md="3" data-items-xs="2" data-effect="slide"
                            data-autoplay="true">
                            <div class="swiper">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide">
                                        <figure class="rounded bg-pale-aqua p-1">

                                            <a href="https://www.wmo.int/" target="_blank">
                                                <img src="https://bmkg.go.id/asset/img/banner/banner-wmo.jpg" alt="WMO">
                                            </a>



                                        </figure>
                                    </div>
                                    <!--/.swiper-slide -->
                                    <div class="swiper-slide">
                                        <figure class="rounded bg-pale-aqua p-1">
                                            <a href="http://epengawasan.bmkg.go.id/wbs/" target="_blank">
                                                <img src="https://bmkg.go.id/asset/img/banner/banner-epengawasan.jpg"
                                                    alt="E-Pengawasan">
                                            </a>
                                        </figure>
                                    </div>
                                    <!--/.swiper-slide -->
                                    <div class="swiper-slide">
                                        <figure class="rounded bg-pale-aqua p-1">
                                            <a href="https://www.lapor.go.id/" target="_blank">
                                                <img src="https://bmkg.go.id/asset/img/banner/Lapor-UKP4.jpg"
                                                    alt="LAPOR!">
                                            </a>
                                        </figure>
                                    </div>
                                    <!--/.swiper-slide -->
                                    <div class="swiper-slide">
                                        <figure class="rounded bg-pale-aqua p-1">
                                            <a href="http://bit.ly/PersepsiWebsiteBMKG">
                                                <img src="https://bmkg.go.id/asset/img/banner/Banner-Survey-BMKG.jpg"
                                                    alt="Survey Layanan Website BMKG">
                                            </a>
                                        </figure>
                                    </div>
                                    <!--/.swiper-slide -->
                                    <div class="swiper-slide">
                                        <figure class="rounded bg-pale-aqua p-1">
                                            <a href="https://www.bmkg.go.id/rb" target="_blank">
                                                <img src="https://bmkg.go.id/asset/img/banner/banner-reformasi-birokrasi.jpg"
                                                    alt="Reformasi Birokrasi BMKG">
                                            </a>
                                        </figure>
                                    </div>
                                    <!--/.swiper-slide -->
                                    <div class="swiper-slide">
                                        <figure class="rounded bg-pale-aqua p-1">
                                            <a href="https://www.bmkg.go.id/profil/?p=stop-pungli-bmkg" target="_blank">
                                                <img src="https://bmkg.go.id/asset/img/banner/saber-pungli.jpg"
                                                    alt="Saber Pungli">
                                            </a>
                                        </figure>
                                    </div>
                                    <!--/.swiper-slide -->
                                    <div class="swiper-slide">
                                        <figure class="rounded bg-pale-aqua p-1">
                                            <a href="http://dataonline.bmkg.go.id/home" target="_blank">
                                                <img src="https://bmkg.go.id/asset/img/banner/banner_data_online.jpg"
                                                    alt="Data Online">
                                            </a>
                                        </figure>
                                    </div>
                                    <div class="swiper-slide">
                                        <figure class="rounded bg-pale-aqua p-1">

                                            <a href="http://puslitbang.bmkg.go.id/jmg" target="_blank">
                                                <img src="https://bmkg.go.id/asset/img/banner/banner-journalMG.jpg"
                                                    alt="Jurnal Meteorologi dan Geofisika">
                                            </a>
                                        </figure>
                                    </div>
                                    <div class="swiper-slide">
                                        <figure class="rounded bg-pale-aqua p-1">
                                            <a href="http://inatews.bmkg.go.id/new/query_gmpqc.php" target="_blank">
                                                <img src="https://bmkg.go.id/asset/img/banner/dataGempa.jpg"
                                                    alt="Data Gempabumi">
                                            </a>
                                        </figure>
                                    </div>
                                    <div class="swiper-slide">
                                        <figure class="rounded bg-pale-aqua p-1">
                                            <a href="https://www.bmkg.go.id/geofisika-potensial/kalkulator-magnet-bumi.bmkg"
                                                target="_blank">
                                                <img src="https://bmkg.go.id/asset/img/banner/kalkulator-magnet.jpg"
                                                    alt="Kalkulator Magnet Bumi">
                                            </a>
                                            </figu>
                                    </div>
                                    <div class="swiper-slide">
                                        <figure class="rounded bg-pale-aqua p-1">
                                            <a href="#">
                                                <img src="https://bmkg.go.id/asset/img/banner/mottobmkg.jpg"
                                                    alt="Motto BMKG">
                                            </a>
                                        </figure>
                                    </div>
                                    <div class="swiper-slide">
                                        <figure class="rounded bg-pale-aqua p-1">
                                            <a href="https://www.bmkg.go.id/profil/?p=maklumat-pelayanan">
                                                <img src="https://bmkg.go.id/asset/img/banner/maklumatpelayanan.jpg"
                                                    alt="Maklumat Pelayanan">
                                            </a>
                                        </figure>
                                    </div>
                                    <div class="swiper-slide">
                                        <figure class="rounded bg-pale-aqua p-1">
                                            <a href="http://lpse.bmkg.go.id" target="_blank">
                                                <img src="https://bmkg.go.id/asset/img/banner/lpse.jpg" alt="LPSE">
                                            </a>
                                        </figure>
                                    </div>
                                    <!--/.swiper-slide -->
                                </div>
                                <!--/.swiper-wrapper -->
                            </div>
                            <!-- /.swiper -->
                        </div>
                        <!-- /.swiper-container -->
                    </div>

                </div>
                <div
                    class="row gx-lg-0 gx-xl-8 mb-5 mb-md-7 mb-lg-14 justify-content-center align-items-center">
                    <div class="col-md-6 col-xl-4">
                        <div class="card shadow-lg card-border-bottom border-soft-primary">
                            <div class="card-body p-4">
                                <img src="./assets/img/icons/lineal/browser.svg"
                                    class="svg-inject icon-svg icon-svg-md text-yellow mb-3" alt="" />
                                <h4>Informasi Publik Setiap Saat</h4>
                                {{-- <p class="mb-2">Nulla vitae elit libero, a pharetra augue. Donec id elit non
                                    mi porta gravida at eget metus cras justo.</p> --}}
                                <a href="#" class="more hover link-primary">Selengkapnya</a>
                            </div>
                            <!--/.card-body -->
                        </div>
                        <!--/.card -->
                    </div>
                    <!--/column -->
                    <div class="col-md-6 col-xl-4">
                        <div class="card shadow-lg card-border-bottom border-soft-primary">
                            <div class="card-body p-4">
                                <img src="./assets/img/icons/lineal/check-list.svg"
                                    class="svg-inject icon-svg icon-svg-md text-yellow mb-3" alt="" />
                                <h4>Informasi Publik Secara Berkala</h4>
                                {{-- <p class="mb-2">Nulla vitae elit libero, a pharetra augue. Donec id elit non
                                    mi porta gravida at eget metus cras justo.</p> --}}
                                <a href="#" class="more hover link-primary">Selengkapnya</a>
                            </div>
                            <!--/.card-body -->
                        </div>
                        <!--/.card -->
                    </div>
                    <!--/column -->
                    <div class="col-md-6 col-xl-4">
                        <div class="card shadow-lg card-border-bottom border-soft-primary">
                            <div class="card-body p-4">
                                <img src="./assets/img/icons/lineal/analytics.svg"
                                    class="svg-inject icon-svg icon-svg-md text-yellow mb-3" alt="" />
                                <h4>Informasi Publik Serta Merta</h4>
                                {{-- <p class="mb-2">Nulla vitae elit libero, a pharetra augue. Donec id elit non
                                    mi porta gravida at eget metus cras justo.</p> --}}
                                <a href="#" class="more hover link-primary">Selengkapnya</a>
                            </div>
                            <!--/.card-body -->
                        </div>
                        <!--/.card -->
                    </div>
                    <!--/column -->

                </div>
                <!-- /.row -->
                <!--/.row -->
                {{-- <div class="row">
                    <div class="col-lg-9 col-xl-8 col-xxl-7 mx-auto text-center">
                        <h2 class="display-6 text-uppercase  mb-3">Layanan BMKG</h2>

                    </div>
                    <!-- /column -->
                </div> --}}
                <!-- /.row -->
                <!-- /.container -->


                <div class="container-xxl bg-green text-white rounded">
                    <div
                        class="row gx-lg-8  gx-xl-12 py-8   justify-content-center align-items-center  ">
                        <div class="col-lg-3 position-relative">
                            <div class="shape bg-dot primary rellax w-17 h-18" data-rellax-speed="1"
                                style="bottom: -2rem; left: -0.7rem;"></div>
                            <figure class="d-none d-lg-block rounded mb-0 "><img src="{{ url('/images/ppid-banner.png') }}" alt="">
                            </figure>
                        </div>
                        <!--/column -->
                        <div class="col-lg-6 mt-12 px-md-8">
                            <h3 class="display-6 mb-4 text-white">Tentang PPID BMKG</h3>
                            <p class="mb-4">Cum sociis natoque penatibus et magnis dis parturient montes,
                                nascetur ridiculus mus. Cras justo odio, dapibus ac facilisis in, egestas eget quam.
                                Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Maecenas faucibus
                                mollis interdum. Maecenas sed diam eget risus varius.</p>

                            <a href="#" class="btn btn-primary rounded-pill  mb-0">Lihat Profil</a>
                        </div>
                        <!--/column -->
                    </div>
                    <div class="row justify-content-center align-items-center py-8 px-4">
                        <div class="col-lg-6 bg-soft-green text-dark text-center rounded p-4 ">
                            <div class="display-6 mb-3">Ajukan Permohonan Informasi</div>
                            <div class="my-2">Lorem ipsum dolor sit amet consectetur adipisicing elit. Ab, eius numquam ipsam officiis molestiae saepe.</div>
                            <div>
                                <a href="#" class="btn btn-primary rounded-pill">Ajukan Permohonan</a>
                            </div>

                        </div>
                    </div>

                </div>

                <div class="container pt-12 pt-md-10 pb-8 pb-md-6">

                    <div class="swiper-container grid-view mb-6" data-margin="30" data-dots="true" data-items-xl="5" data-items-md="3" data-items-xs="2">
                      <div class="swiper">
                        <div class="swiper-wrapper">
                          <div class="swiper-slide">
                            <figure class="rounded mb-6"><img src="../../assets/img/photos/pd7.jpg" srcset="../../assets/img/photos/pd7@2x.jpg 2x" alt="" /><a class="item-link swiper-no-swiping" href="../../assets/img/photos/pd7-full.jpg" data-glightbox data-gallery="projects-group"><i class="uil uil-focus-add"></i></a></figure>
                            <div class="project-details d-flex justify-content-center flex-column">
                              <div class="post-header">
                                <h2 class="post-title h3"><a href="../../single-project.html" class="link-dark">Cras Fermentum Sem</a></h2>
                                <div class="post-category text-ash">Stationary</div>
                              </div>
                              <!-- /.post-header -->
                            </div>
                            <!-- /.project-details -->
                          </div>
                          <!--/.swiper-slide -->
                          <div class="swiper-slide">
                            <figure class="rounded mb-6"><img src="../../assets/img/photos/pd8.jpg" srcset="../../assets/img/photos/pd8@2x.jpg 2x" alt="" /><a class="item-link swiper-no-swiping" href="../../assets/img/photos/pd8-full.jpg" data-glightbox data-gallery="projects-group"><i class="uil uil-focus-add"></i></a></figure>
                            <div class="project-details d-flex justify-content-center flex-column">
                              <div class="post-header">
                                <h2 class="post-title h3"><a href="../../single-project2.html" class="link-dark">Mollis Ipsum Mattis</a></h2>
                                <div class="post-category text-ash">Magazine, Book</div>
                              </div>
                              <!-- /.post-header -->
                            </div>
                            <!-- /.project-details -->
                          </div>
                          <!--/.swiper-slide -->
                          <div class="swiper-slide">
                            <figure class="rounded mb-6"><img src="../../assets/img/photos/pd9.jpg" srcset="../../assets/img/photos/pd9@2x.jpg 2x" alt="" /><a class="item-link swiper-no-swiping" href="../../assets/img/photos/pd9-full.jpg" data-glightbox data-gallery="projects-group"><i class="uil uil-focus-add"></i></a></figure>
                            <div class="project-details d-flex justify-content-center flex-column">
                              <div class="post-header">
                                <h2 class="post-title h3"><a href="../../single-project3.html" class="link-dark">Ipsum Ultricies Cursus</a></h2>
                                <div class="post-category text-ash">Packaging</div>
                              </div>
                              <!-- /.post-header -->
                            </div>
                            <!-- /.project-details -->
                          </div>
                          <!--/.swiper-slide -->
                          <div class="swiper-slide">
                            <figure class="rounded mb-6"><img src="../../assets/img/photos/pd10.jpg" srcset="../../assets/img/photos/pd10@2x.jpg 2x" alt="" /><a class="item-link swiper-no-swiping" href="../../assets/img/photos/pd10-full.jpg" data-glightbox data-gallery="projects-group"><i class="uil uil-focus-add"></i></a></figure>
                            <div class="project-details d-flex justify-content-center flex-column">
                              <div class="post-header">
                                <h2 class="post-title h3"><a href="../../single-project.html" class="link-dark">Inceptos Euismod Egestas</a></h2>
                                <div class="post-category text-ash">Stationary, Branding</div>
                              </div>
                              <!-- /.post-header -->
                            </div>
                            <!-- /.project-details -->
                          </div>
                          <!--/.swiper-slide -->
                          <div class="swiper-slide">
                            <figure class="rounded mb-6"><img src="../../assets/img/photos/pd11.jpg" srcset="../../assets/img/photos/pd11@2x.jpg 2x" alt="" /><a class="item-link swiper-no-swiping" href="../../assets/img/photos/pd11-full.jpg" data-glightbox data-gallery="projects-group"><i class="uil uil-focus-add"></i></a></figure>
                            <div class="project-details d-flex justify-content-center flex-column">
                              <div class="post-header">
                                <h2 class="post-title h3"><a href="../../single-project2.html" class="link-dark">Ipsum Mollis Vulputate</a></h2>
                                <div class="post-category text-ash">Packaging</div>
                              </div>
                              <!-- /.post-header -->
                            </div>
                            <!-- /.project-details -->
                          </div>
                          <!--/.swiper-slide -->
                          <div class="swiper-slide">
                            <figure class="rounded mb-6"><img src="../../assets/img/photos/pd12.jpg" srcset="../../assets/img/photos/pd12@2x.jpg 2x" alt="" /><a class="item-link swiper-no-swiping" href="../../assets/img/photos/pd12-full.jpg" data-glightbox data-gallery="projects-group"><i class="uil uil-focus-add"></i></a></figure>
                            <div class="project-details d-flex justify-content-center flex-column">
                              <div class="post-header">
                                <h2 class="post-title h3"><a href="../../single-project3.html" class="link-dark">Porta Ornare Cras</a></h2>
                                <div class="post-category text-ash">Branding</div>
                              </div>
                              <!-- /.post-header -->
                            </div>
                            <!-- /.project-details -->
                          </div>
                          <!--/.swiper-slide -->
                        </div>
                        <!--/.swiper-wrapper -->
                      </div>
                      <!-- /.swiper -->
                    </div>
                    <!-- /.swiper-container -->
                  </div>




            </div>


        </section>
        <!-- /section -->
    </div>
    <!-- /.content-wrapper -->
    <footer class="bg-light">
        <div class="container py-13 py-md-15">
            <div class="row gy-6 gy-lg-0">
                <div class="col-md-4 col-lg-3">
                    <div class="widget">
                        <img class="mb-4" src="./assets/img/logo-dark.png"
                            srcset="./assets/img/logo-dark@2x.png 2x" alt="" />
                        <p class="mb-4">© 2021 Sandbox. <br class="d-none d-lg-block" />All rights reserved.
                        </p>
                        <nav class="nav social ">
                            <a href="#"><i class="uil uil-twitter"></i></a>
                            <a href="#"><i class="uil uil-facebook-f"></i></a>
                            <a href="#"><i class="uil uil-dribbble"></i></a>
                            <a href="#"><i class="uil uil-instagram"></i></a>
                            <a href="#"><i class="uil uil-youtube"></i></a>
                        </nav>
                        <!-- /.social -->
                    </div>
                    <!-- /.widget -->
                </div>
                <!-- /column -->
                <div class="col-md-4 col-lg-3">
                    <div class="widget">
                        <h4 class="widget-title  mb-3">Get in Touch</h4>
                        <address class="pe-xl-15 pe-xxl-17">Moonshine St. 14/05 Light City, London, United Kingdom
                        </address>
                        <a href="mailto:#" class="link-body">info@email.com</a><br /> 00 (123) 456 78 90
                    </div>
                    <!-- /.widget -->
                </div>
                <!-- /column -->
                <div class="col-md-4 col-lg-3">
                    <div class="widget">
                        <h4 class="widget-title  mb-3">Learn More</h4>
                        <ul class="list-unstyled text-reset mb-0">
                            <li><a href="#">About Us</a></li>
                            <li><a href="#">Our Story</a></li>
                            <li><a href="#">Projects</a></li>
                            <li><a href="#">Terms of Use</a></li>
                            <li><a href="#">Privacy Policy</a></li>
                        </ul>
                    </div>
                    <!-- /.widget -->
                </div>
                <!-- /column -->
                <div class="col-md-12 col-lg-3">
                    <div class="widget">
                        <h4 class="widget-title  mb-3">Our Newsletter</h4>
                        <p class="mb-5">Subscribe to our newsletter to get our news & deals delivered to you.
                        </p>
                        <div class="newsletter-wrapper">
                            <!-- Begin Mailchimp Signup Form -->
                            <div id="mc_embed_signup2">
                                <form
                                    action="https://elemisfreebies.us20.list-manage.com/subscribe/post?u=aa4947f70a475ce162057838d&amp;id=b49ef47a9a"
                                    method="post" id="mc-embedded-subscribe-form2" name="mc-embedded-subscribe-form"
                                    class="validate " target="_blank" novalidate>
                                    <div id="mc_embed_signup_scroll2">
                                        <div class="mc-field-group input-group form-floating">
                                            <input type="email" value="" name="EMAIL"
                                                class="required email form-control" placeholder="Email Address"
                                                id="mce-EMAIL2">
                                            <label for="mce-EMAIL2">Email Address</label>
                                            <input type="submit" value="Join" name="subscribe"
                                                id="mc-embedded-subscribe2" class="btn btn-primary ">
                                        </div>
                                        <div id="mce-responses2" class="clear">
                                            <div class="response" id="mce-error-response2" style="display:none">
                                            </div>
                                            <div class="response" id="mce-success-response2"
                                                style="display:none"></div>
                                        </div>
                                        <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                                        <div style="position: absolute; left: -5000px;" aria-hidden="true"><input
                                                type="text" name="b_ddc180777a163e0f9f66ee014_4b1bcfa0bc" tabindex="-1"
                                                value=""></div>
                                        <div class="clear"></div>
                                    </div>
                                </form>
                            </div>
                            <!--End mc_embed_signup-->
                        </div>
                        <!-- /.newsletter-wrapper -->
                    </div>
                    <!-- /.widget -->
                </div>
                <!-- /column -->
            </div>
            <!--/.row -->
        </div>
        <!-- /.container -->
    </footer>
    <div class="progress-wrap">
        <svg class="progress-circle svg-content" width="100%" height="100%" viewBox="-1 -1 102 102">
            <path d="M50,1 a49,49 0 0,1 0,98 a49,49 0 0,1 0,-98" />
        </svg>
    </div>
    <script src="./assets/js/plugins.js"></script>
    <script src="./assets/js/theme.js"></script>
</body>

</html>
