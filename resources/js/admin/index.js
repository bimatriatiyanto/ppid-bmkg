import './admin-user';
import './profile-edit-profile';
import './profile-edit-password';
import './page';
import './category';
import './document';
import './award';
