import AppForm from '../app-components/Form/AppForm';

Vue.component('document-form', {
    mixins: [AppForm],
    props: [
        'categories'
    ],
    data: function() {
        return {
            form: {
                title:  '' ,
                categories:  '' ,

            },
            mediaCollections: ["berkas"],
        }
    }

});
