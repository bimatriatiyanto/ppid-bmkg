import AppListing from '../app-components/Listing/AppListing';

Vue.component('award-listing', {
    mixins: [AppListing]
});