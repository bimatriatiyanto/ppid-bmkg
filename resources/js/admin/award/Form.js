import AppForm from '../app-components/Form/AppForm';

Vue.component('award-form', {
    mixins: [AppForm],
    data: function() {
        return {
            form: {
                title:  '' ,
                slug:  '' ,
                description:  '' ,
                enabled:  false ,

            },
            mediaCollections: ["penghargaan"],
        }
    }

});
