<?php
namespace App\Models;

use Spatie\MediaLibrary\HasMedia;
use Illuminate\Database\Eloquent\Model;
use Brackets\Media\HasMedia\ProcessMediaTrait;
use Brackets\Media\HasMedia\HasMediaThumbsTrait;
use Brackets\Media\HasMedia\AutoProcessMediaTrait;
use Brackets\Media\HasMedia\HasMediaCollectionsTrait;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Document extends Model implements HasMedia
{
    use ProcessMediaTrait;
    use AutoProcessMediaTrait;
    use HasMediaCollectionsTrait;
    use HasMediaThumbsTrait;

    protected $table = 'documents';

    protected $fillable = [
        'title',
        'category_id',

    ];


    protected $dates = [
        'created_at',
        'updated_at',

    ];

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('berkas')
            ->useDisk('public')
            ->accepts('application/pdf')
            ->maxNumberOfFiles(1)
            ->singleFile();
    }

    public function registerMediaConversions(Media $media = null): void
    {
        $this->autoRegisterThumb200();
    }

    protected $appends = ['resource_url'];

    /* ************************ ACCESSOR ************************* */

    public function getResourceUrlAttribute()
    {
        return url('/admin/documents/'.$this->getKey());
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}
